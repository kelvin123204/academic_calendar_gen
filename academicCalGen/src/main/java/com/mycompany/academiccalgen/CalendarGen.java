/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.academiccalgen;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.io.TimezoneAssignment;
import biweekly.property.RecurrenceRule;
import biweekly.util.Duration;
import biweekly.util.Frequency;
import biweekly.util.Recurrence;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;



/**
 *
 * @author kelvi
 */
public class CalendarGen {
    private String eventTitle;
    private String location;
    private String desc;
    private Calendar startTime;
    private Duration duration;

    @Override
    public String toString() {
        return "CalendarGen{" + "eventTitle=" + eventTitle + ", location=" + location + ", desc=" + desc + ", startTime=" + startTime.toString() + ", duration=" + duration.toString() + '}';
    }
    
    void calendarGen() throws IOException{
        ICalendar ical = new ICalendar();
        String globalId = "Asia/Hong_Kong";
        TimeZone tz = TimeZone.getTimeZone(globalId);
        TimezoneAssignment hk = new TimezoneAssignment(tz, globalId);
        ical.getTimezoneInfo().setDefaultTimezone(hk);
        VEvent event = new VEvent();
        // set the event title
        event.setSummary(eventTitle);
        event.setLocation(location);
        event.setDescription(desc);
        // set the event start time
        event.setDateStart(startTime.getTime());
        // set the event duration
        event.setDuration(duration);
        // set the event to be repeated weekly until 27/04/2019
        RecurrenceRule rrule = new RecurrenceRule(new Recurrence.Builder(Frequency.WEEKLY).until(new Calendar.Builder().setDate(2019, Calendar.APRIL, 27).build().getTime()).build());
        event.setRecurrenceRule(rrule);     
        ical.addEvent(event);
        String fileName = JOptionPane.showInputDialog("Filename: ");
        if(fileName.compareTo("") == 0){
            JOptionPane.showMessageDialog(null,"The filename cannot be empty");
            return;
        }
        fileName += ".ics";
        File file = new File(fileName);
        Biweekly.write(ical).go(file);
    }
    
    
    void setEventTitle(String title){
        this.eventTitle = title;
    }
    
    void setLocation(String location){
        this.location = location;
    }
    
    void setDesc(String message){
        this.desc = message;
    }
    
    void setStartTime(int year, int month, int day, int hour, int minute){
        // jan = 0, feb = 1, etc...
        Calendar.Builder calBuild = new Calendar.Builder();
        calBuild.setDate(year, month, day);
        calBuild.setTimeOfDay(hour, minute, 0);
        this.startTime = calBuild.build();
    }
    
    void setDuration(int minutes){
        this.duration = Duration.builder().minutes(minutes).build();
    }
    
}
